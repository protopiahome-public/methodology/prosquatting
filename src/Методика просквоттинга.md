# Просквоттинг. Оживление заброшенных зданий силами локальных сообществ.


## Методические рекомендации. 


# Глоссарий

Локальное сообщество - целостная культура местного сообщества, сформировавшаяся как результат жизнедеятельности в определенных природных условиях

Калуцков В. Н. Ландшафт в культурной географии.—М.:Новый Хронограф, 2008

Заброшенное здание / помещение -  это прежде всего помещение, которое забросили, а уже в последующем это разрушение, ветхость или иное.

Просквоттинг


# Что это?

Просквоттинг — это занятие заброшенных зданий и помещений с целью облагораживания городской среды, с добровольного разрешения, а иногда и с поддержкой собственника здания. 

Во многих городах есть такие забытые места, с которыми муниципальные структуры не могут ничего сделать из-за прав собственности, а владельцы не способны развивать в силу различного рода обстоятельств. Это заброшенные или недостроенные здания, а порой даже целые заброшенные кварталы. Забытые бывшими (или текущими) собственниками, такие места часто становятся пристанищем местных бомжей, алкоголиков, наркоманов и других маргиналов.

При этом заброшенность является проблемой и для собственника, который уже десятилетиями не может ничего сделать с подобной инфраструктурой — он тратит деньги на регулярный ремонт, обслуживание, платит налог на землю и имущество. Это здание является для него нерентабельным, поскольку выстроить коммерческую схему не представляется возможным. А значит, здание просто стареет, теряет свой исторический вид, ветшает и ломается от осадков и погодных условий.

Часто такие места занимают маргиналы, занимаясь сквоттингом, то есть занятием помещения без ведома его владельца. Движение просквоттинга предлагает альтернативу: передачу места сообществу городских активистов. Что дает законную и легальную схему взаимодействия владельцев с горожанами. Позволяет выстроить мост в отношениях между различными сторонами в этом сложном вопросе и в итоге облагородить городскую среду.


# История вопроса.

По всему миру в последняя время встала проблема реновации зданий широко востребованных в индустриальную эпоху и абсолютно не востребованных в современную.

Историческим примером просквоттинга является испанское пространство "Can Batllo". Сообщество городских активистов активно боролось, а затем стало сотрудничать с муниципальными властями города за возможность облагораживать городское пространство. Стоит заметить, что они боролись не против системы, а скорее трансформировали собственную жизнь в желательном направлении (включая, кстати, общение с государством).

 

Кан Бадьё расположено в огромной старой фабрике и создается силами волонтеров. Оно направлено на творческое развитие людей, посещающих площадку. До появления волонтерского движения это было заброшенное строение. После занятия помещения и переговоров с муниципалитетом, Кан Бадьё стало одним из центров общественной жизни города, тем самым изменив его облик.

 

В 2018 году в Москве сейчас тем же самым занимается инициативная группа «Дом Протопии» и родственные ей группы. В первую очередь они, разумеется, ищут пространство для своей деятельности, но при этом также развивают методики новой экономики, в том числе и описанный выше.

Эта группа ставит себе задачу найти заброшенное здание и договориться с его собственником о просквоттинге. После чего сделать из этого здания общественную площадку, пригласить туда различные группы с интересными мероприятиями и мастерскими, оживить социальную среду вокруг этого места. Тем самым создать пример такого места и разработать методологию их создания, чтобы другие люди могли этим воспользоваться.


# Кто же эти просквоттеры и зачем они этим занимаются?

За этим скрывается вполне понятное явление, если посмотреть со стороны городских активистов. У большинства из них нет постоянной общественной площадки, в которой они могли бы организовывать свою деятельность, встречаться, общаться и отдыхать. Денег на организацию коммерческой деятельности, чтобы отбивать арендную плату, у них тоже немного, поэтому они ищут возможность найти любое такое общественное пространство, где они могли бы заниматься своей деятельностью.

 

В современном мире часто такими площадками становятся общественные кафе, арт-пространства и торговые центры, где они встречаются, общаются и работают. Это уже созданные государством или предпринимателями площадки. Но в данном случае идёт речь об инициативе "снизу", когда сами активисты предлагают создать такое пространство, предназначенное для тех же самых вещей.

 


# Что такое заброшенное помещение?

Заброшенное помещение это прежде всего помещение, которое забросили, а уже в последующем это разрушение, ветхость или иное.

Заброшенность помещения можно оценить по следующим признакам:



1. Несколько месяцев или лет в нем никто не находиться
    1. или находятся только маргиналы.
2. Здание ветшает и разваливается.
3. В здании видны следы пыли, плесени и неухоженности.
4. Здание отключили от коммуникаций.
5. На здании навешены строительные леса, но работы не проводятся в течении нескольких месяцев.


# Что это даст собственнику

Просквоттинг не нарушает прав владельца — наоборот, это взаимовыгодное сотрудничество городских сообществ и владельцев заброшенных зданий, направленное на решение проблемы запустения зданий и ухудшения городской среды. 

 

Просквоттеры предлагают возможность собственнику на законных условиях решить сразу множество проблем с таким имуществом. Это обустройство и ремонт внутреннего пространства здания. Они привлекают волонтёров, делают ремонт своими силами и следят за помещением.



1. Не тратиться на охрану 
2. Не будет претензии со стороны города касательно заброшенности
3. Не будет особенностей того, что зимой никто не живет - набирает влага, образуется плесень, сосульки.
4. Летом просквоттеры могут следить за придомовой территорией - деревья, трава.


# Метод “Просквоттинга”



1. Примите осознанное решение, что вы этим занимаетесь!
2. Первый шаг. Основная идея и команда проекта.
3. Привлекая внимание.
4. Разведка. Поиск заброшенных зданий.
5. Контакт.
6. Согласие и старт.
7. План и смета.
8. Привлечение ресурсов (сбор средств и материалов).
9. Сообщество.
10. Провести сбор волонтеров.
11. Провести ремонтные работы и облагораживающие мероприятия.
12. Экономическая модель
13. Место действия. Открыть общественную площадку.


## 1. Примите осознанное решение!

В отличии от классических методов запуска бизнеса или некоммерческого предприятия, “Просквоттинг” дает возможность запустить именно социально-ориентированный проект. С одной стороны это позволяет привлечь значительные ресурсы и дает высокую мотивацию у участников, с другой стороны это накладывает ожидания со стороны внешнего окружения такого проекта. 

Перед тем как запускать проект, особенно проект общественного центра, общественной площадки, социально-ориентированный проект или социально-предпринимательский проект важно принять добровольное и осознанное решение о его запуске.

Оживление города и активное изменение социальной среды будут требовать мужества и настойчивости. Возможно, вас будут считать “чудиком” (фриком), но возможно вы кардинально измените жизнь в своем районе, городе или регионе.


## 2. Первый шаг. Основная идея и команда проекта. 

После того как вы приняли осознанное решение, пора браться за карандаш, компьютер или печатную машинку.

У вас уже есть мотив изменить мир вокруг вас, но необходимо ответить на вопрос “Что это будет и как вы собираетесь делать?”. Здесь нам поможет описание основных идей проекта в виде законченного образа или набора зарисовок.



1. Детально продумайте, что должно получиться, опишите это в одном двух абзацах текста или сделайте наброски карандашом. Придумайте название проекта. Этот материал будет отправной точкой для последующих разговоров о содержании вашего проекта.
2. На основании идеи опишите необходимые площади и оборудование. Продумайте архитектурный план (конечно он будет немного фантастическим).
3. Подумайте и опишите: Кто будет отвечать за содержательную часть, а кто за организацию деятельности. Только вы или вы будете привлекать знакомых и партнеров?
    1. Держатели помещения - люди и команды, постоянно присутствующие в помещении и несущие большую часть ответственности за него. Отвечают за организацию деятельности.
    2. Контент-держатели - проводят в помещении деятельность, регулярную и нерегулярную.
4. Если вы понимаете, что у вас не хватит сил, то привлекайте партнеров.
5. Проведите предварительные переговоры с держателями помещения и контент-держателями.
6. Обсудите способы помощи проекту, с учетом компетенций и ресурсов ваших знакомых и партнеров, кто готов участвовать в проекте. 
7. Координируйте их деятельность по мере поступления новой информации и появлении новых задач.

В итоге у вас получиться описание вашего проекта, а также команда которая способна начать его воплощать.

В книге “Место действия” в приложениях приводятся две методики по проектированию такого места.


## 3. Привлекая внимание.

После описания проекта создайте лендинг или группу в социальных сетях с подробным описанием проекта. Желательно сделать и то и то, но если нет сил или умений, то желательно хотя бы сделать что-то одно.

На лендинге / в группе изложите основную идею, требуемые характеристики помещения, команду проекта и контактные данные.

Расскажите, но не сильно навязчиво о проекте своим ближайшим знакомым и вышлите им ссылку на группу или лендинг.

По мере получения новой информации и появления ключевых событий делайте пресс-релиз: описание проекта на данном этапе. Под ключевыми событиями здесь имеется в виду: изменения в команде, появление интересного помещения, появление договоренностей и т.п.

Распространяйте информацию по мере возможностей, через группы в социальных сетях, интернет-издания, районные и тематические чаты, районные газеты и группы в соц. сетях.

При желании ходите на тематические мероприятия и конференции. Это увеличит шанс получения нужного контакта и повысит известность проекта.

Публичное освещение проекта необходимо для создания сообщества вокруг проекта, что важно на этапе облагораживания здания. Проекты по облагораживанию городской среды невозможны в одиночку или только в бизнес-логике, это всегда социально-ориентированные проекты.


## 4. Разведка. Поиск заброшенных зданий.

Поиск помещения может проводиться множеством различных способов, здесь важно находить именно заброшенные помещения. Важно, что заброшенное помещение не означает его ветхость. 

Ниже приведены основные способы поиска помещения:



1. Поиск через тематические сайты, например 
    1. на сайте Викимапия: [https://vk.cc/8G6JJT](https://vk.cc/8G6JJT) 
    2. на сайте УрбанТрип: [https://urban3p.ru/objects](https://urban3p.ru/objects) 
    3. на сайте Архнадзора: [https://redbook.archnadzor.ru/](https://redbook.archnadzor.ru/) 
2. Поиск через сайты продажи и аренды недвижимости.
3. Прогулки по городу и поиск неиспользуемых строений (см. признаки заброшенности строения).
4. Разговор с управой районов, местными депутатами и мэрией города.
5. Разговор с экспертами в области архитектуры и урбанистики.
6. Посещение тематических конференций по недвижимости 
7. Посещение клубов предпринимателей и схожих мероприятий.
8. Общение в коллективах и чатах связанных с филантропией, НКО и социальным предпринимательством.

После нахождения интересного объекта необходимо провести доразведку:



1. Осмотреть заброшенное здание с целью получения  и подтверждения актуальной информации.
2. Провести фотосъемку здания. Это необходимо как для подтверждения заброшенности, так и для дальнейших переговоров.
3. По возможности описать степень заброшенности и проблемные места.
4. Нарисовать первичную схему здания. Это понадобиться если не будет возможности получения плана БТИ.

На этом этапе по возможности привлекайте различного рода экспертов и общественных деятелей: архитекторов, строителей, инженеров, урбанистов, локальных лидеров общественного мнения и т. п.

[https://lastday.club/zabroshennyie-obektyi-pamyatka-po-obsledovaniyu/](https://lastday.club/zabroshennyie-obektyi-pamyatka-po-obsledovaniyu/)  


## 5. Контакт. 

При правильных действиях по разведке и распространению информации у проекта появляется репутация и имидж. Он становиться понятным и узнаваемым. Пора переходить к активным действиям общению с собственниками заброшенных помещений и стратегическими партнерами.

Если вы знаете где расположено здание, то его владельца найти не так уж и сложно. Вся эта информация находиться в открытых источниках или доступна по запросу. 

Чтобы получить контакт владельца вы можете:



1. Спросить его у охраны здания, если таковая имеется.
2. Получить контакты через сайт Росреестра: [https://efimovlaw.ru/vypiski-po-rublyu/](https://efimovlaw.ru/vypiski-po-rublyu/)
3. При наличии знания об организации-владельце, через ИНН организации. 
4. Получить контакт через личное знакомство на конференциях, форумах или через тематические чаты.
5. Если здание принадлежит госструктурам:
    1. Обратись через форму электронных обращений на сайте ведомства.
    2. Договоритесь по одной из схем предоставления государственной собственности: [https://drive.google.com/open?id=1st12FKzT5N-T-tEsvdMII89AuPwOp8N0](https://drive.google.com/open?id=1st12FKzT5N-T-tEsvdMII89AuPwOp8N0)


## 6. Согласие и старт.

После получения контакта важно рассказать собственнику о вашем проекте и ваших планах об облагораживании места. Для этого необходимо созвониться, списаться с собственником, выйти с ним на контакт и выслать материалы о проекте. Если нет кардинальных противоречий и прямого отказа от сотрудничества, далее назначить встречу, где рассказать о проекте и обсудить условия сотрудничества. 

На этом этапе важно как выслушать про проблемы собственника с помещением и понять, что мешает развивать здание силами владельца. Обговорить технические, архитектурные и юридические вопросы. Детально изложить, в чем суть просквоттинга. Предложить свою помощь, если это возможно по устранению проблем.

Совместно найти оптимальную схему сотрудничества:



1. Меценатство. Если собственник одобряет деятельность, которую мы ведем и планируем вести,  и он готов предоставить Помещение в дар или бесплатный доступ в него.
2. Облагораживание. Если собственник не знает, что делать с помещением и готов дать его под управление команде, которая приведет его в порядок. Минимальная аренда: 1 рубль за 1 кв. метр + оплата всех коммунальных услуг + ремонт/облагораживание. 
3. Доля. Если собственнику нравиться направление деятельности: “Культурные центры и Общественные площадки” и он готов предоставить помещение по льготным условиям за долю в бизнесе. Доля в бизнесе + оплата коммунальных услуг. Для этого требуется организовать с собственником совместное предприятие.
4. Аренда с каникулами на ремонт. Собственник готов предоставлять помещение за деньги по стандартной арендной ставке. Вариант рассматривается только при длительных арендных каникулах.

Попросите у собственника копию плана БТИ и свидетельства о собственности.

Важно не вводить себя и собственника в заблуждение относительно проекта и помещения. Провести детальный осмотр помещения с собственником, составить список необходимых работ и оценить сроки по устранению ключевых проблем. 

В рамках просквоттинга может проводиться ремонт помещения для осуществления целевой деятельности, в частности отделку, проведение интернета и электричества, внутренняя перепланировка. Проблемы инфраструктурного характера, как ремонт капитальные коммуникации, такие как канализация, проведение электричества (например трехфазного) и т. п. должны решаться совместно с собственником.

По итогам данного этапа у всех сторон есть четкое понимание как будет проходить процесс и кто за него ответственный. Изначальный проект уточняется, в него вносятся корректировки с учетом конкретного помещения. Заключить договор безвозмездной передачи или аренды, с условием проведение ремонтных работ и облагораживания.


## 7. План и смета (презентации).

Облагораживание здания это процесс его ремонта, реставрации и приведения в пригодный для деятельности вид. 

Ремонт и реставрация здания требует больших вложений времени, сил и ресурсов. Поэтому, получив согласие и уточнив проект, следует написать план действий по ролям, составить список необходимых ресурсов (смету проекта) и установить точные сроки. Если смета проекта составлена верно, то на этапе привлечения ресурсов не должно возникнуть больших сложностей с поиском ресурсов.

План позволит вам прогнозировать запуск проекта и контролировать его развитие. Ни один план никогда не выполнялся, но без него вы теряете ориентир в своих действиях.

По итогам этого этапа ваш проект переходит из состояния идеи собственно в состояние проекта. Он имеет сроки и цель.


## 8. Привлечение ресурсов.

(Выдержки из книги: “Том Сойер Фест. Как люди берегут город.”)

Источники ресурсов в каждом городе и даже относительно крупном селе стандартны:

Во-первых, это местные, региональные, макрорегиональные представительства или франшизы крупных компаний. Ряд населённых пунктов обеспечивали краской, материалами и инструментами региональные представители и отделения компаний “Тиккурила”,  “Леруа Мерлен”, компания “Неомид”. Также, например, в нескольких городах промокодами на пиццу для волонтёров нам помогали местные франчайзи “Додо пиццы”.

Нужно понимать, что в таких крупных компаниях довольно долго идут согласования, поэтому нужно обращаться к ним как можно раньше. Даже если дом ещё не выбран, с ними можно связаться и сообщить о своих намерениях. Предварительное знакомство точно не помешает. Важно чётко соблюдать с партнёрами достигнутые договорённости. Если вы заручились их предварительной поддержкой и договорились встретиться, например, через месяц, чтобы проговорить более конкретные условия сотрудничества, сделайте это. Если вместо этого вы появитесь через три месяца, то к вам будет отношение как к ненадёжному партнёру.

Также крупный бизнес периодически проводит в разных регионах грантовые конкурсы. 

Второй важный источник ресурсов — это местный бизнес разного масштаба. Тут больше возможностей для импровизации в партнёрских отношениях. В общем, в местном бизнесе порой таятся самые неожиданные ресурсы, а его представители могут стать вам не только партнёрами, но и друзьями.

Третий источник ресурсов, с которого у многих обычно начинаются мысли о подобных проектах, это поддержка государства в виде грантов или субсидий из бюджетов разного уровня — федерального, регионального или муниципального.

Однако необходимо понимать, что привлечение грантов сопровождается написанием отчетов, что в итоге может отвлечь от основной деятельности.

Последний, но немаловажный источник благосостояния — это вклад людей, сопереживающих вашей деятельности. Важный момент: почему стоит принимать даже микропожертвования, но на понятные задачи с понятными сметами. Это ещё одна возможность для людей, которые не могут по тем или иным причинам быть волонтёрами на лесах или оказаться полезными какими-то компетенциями, поучаствовать в общей деятельности.

И напоследок одна важная вещь, которую нужно осознать. Никогда не кладите яйца в одну корзину.


## 9. Сообщество. 

При правильных действиях на первых этапах вокруг проекта формируется сообщество заинтересованное в облагораживании этого здания. Это сообщество является, как источником различного рода ресурсов, так и потребителями дальнейших продуктов общественной площадки. Однако такое отношение губит на корню саму идею сообщества, как высокосвязной группы. Поэтому:

**Важно! Сообщество это прежде всего Живые Люди, а не потребители или источник ресурсов.**

Как поступать в таком случае? Выстраивать человеческие отношения, тем самым формируя общий интерес и социальные связи между участниками сообщества.

На данном этапе необходимо провести несколько общих сборов с активными участниками команды и сообщества. Начать активно информировать участников сообщества и заинтересованных лиц о начале работ по ремонту, с просьбой о помощи в конкретных делах и запросом на конкретные ресурсы.


## 10. Волонтеры.

Если правильно транслировать задачи в сообщество, то в нем образуются волонтеры и просто люди сопереживающие проекту. Эти люди при правильном к ним отношении будут готовы помочь вам с вашим проектом, а также доформировать команду, если какие-то роли еще не заняты.

Не бойтесь привлекать жителей р-на в котором проводиться облагораживание здания. Красивый, ухоженный вид строений и начилие социально-ориентированных проектов позитивно сказывается на жизни горожан. 

Наличие юр. лица упрощает работу с волонтерами, так как волонтерам могут быть выписаны волонтерские книжки, что для некоторых студентов является немаловажным бонусом. 

Волонтер это наемный работник. Это человек работающий на добровольных началах, а значит в любой момент способный выйти из вашего проекта и отказаться от помощи вам. Волонтер прежде всего заинтересован в своем личном развитии и в том, чтобы ваш проект был.

Поэтому прежде чем давать задачи волонтеру, необходимо детально обсудить условия его участия. А также выявить его компетентность для задачи которая будет ему ставиться.

Не бойтесь предлагать волонтерам и заинтересованным лицам нематериальные награды. Награды это не покупка их труда, это вознаграждение за их добровольную помощь.

Ниже приведен алгоритм работы с волонтерами:



1. Встретьтесь, обсудите проект, конкретные задачи в рамках проекта и общие условия.
2. Обсудите Награду 
    1. В ходе выполнения: В виде приобретаемого в ходе выполнения опыта, доступа к информации и связям.
    2. При выполнении задачи: В виде льготных условий на Услуги проекта.
3. Если условия устраивают волонтера, получите четкое, явное согласие на участие в проекте (письменное или устное) и назначьте четкие сроки по выполнению задач.
4. Предоставьте ресурсы и инструменты для проведения работ
5. Регулярно встречайтесь, созванивайтесь по телефону или списывайтесь в чате для координации действий. 
5. После выполнения задачи, передайте заслуженную Награду и обсудите дальнейшее сотрудничество.


## 11. Ремонт и реставрация здания.

Ремонт и реставрационные работы важно проводить поэтапно, от более сложных, капитальных работ к отделочным и менее сложным.

Разбейте работы на несколько этапов, таких, что при выполнении этапа здание становиться более пригодным для осуществления деятельности. 

В идеале в первые несколько этапов должны войти работы по капитальному ремонту перекрытий, кровли, электричества и канализации. Без этих работ проведение последующих этапов трудновыполнимо или вообще бессмысленно. Также включите в первый этап общую уборку и обмер здания.

После проведения этого этапа в здании уже можно будет проводить мероприятия или хотя бы проводить сборы.

На последующих этапах проводите косметический ремонт и отделочные работы. Это позволит привести здание в благопристойный вид. 


## 12. Экономическая модель


## 13. Место действия.

Постепенно облагораживание места и проведения разного рода мероприятий вовлекает жителей и активистов в развитие такой площади. Это приводит к постепенной трансформации места.

Запустение это признак того, что у места нет ответственного (в книге “Место действие” его называют хозяином) и приведенные выше мероприятия преимущественно направлены на взятие коллективной ответственности за место.  


# Примеры и кейсы

Российские и не только.



*   Камбодье
*   Как открыть семейный бизнес в заброшенном доме в центре Москвы -[ https://www.vedomosti.ru/lifestyle/articles/2014/02/14/kak-otkryt-semejnyj-biznes-v-zabroshennom-dome-v-centre](https://www.vedomosti.ru/lifestyle/articles/2014/02/14/kak-otkryt-semejnyj-biznes-v-zabroshennom-dome-v-centre)
*   Дебаркадер (купить в провинции дешево, привезти в Москву) -[  https://vk.com/domnavode](https://vk.com/domnavode)
*   Дом Средний (продать несколько квартир, купить одну большую) -[ ? https://vk.com/dom_sredny](https://vk.com/dom_sredny)
*   Семейные школы (если ряд кейсов, когда им предоставляли помещение бесплатно) -[ https://vk.com/alternativnoeobrazovanie](https://vk.com/alternativnoeobrazovanie)


# Литература



1. Книга “Место Действия”.
2. «ТОМ СОЙЕР ФЕСТ»: как любить город не только на словах.
3. АНДРЕЙ КОЧЕТКОВ. Как люди берегут историю, а история – людей
4. “КОД” модель культурного центра для моногородов.
5. При осмотре заброшенного помещения [https://lastday.club/zabroshennyie-obektyi-pamyatka-po-obsledovaniyu/](https://lastday.club/zabroshennyie-obektyi-pamyatka-po-obsledovaniyu/) 


# Контактная информация.

Подробности о проекте просквоттинга вы можете узнать по адресу: [http://offer.protopia-home.ru](http://offer.protopia-home.ru)

По всем вопросам:



*   Лично Моросеев Федор: телеграмме: @princenoire , телефон: +79055010613 , сеть “Вконтакте” [https://vk.com/princenoire](https://vk.com/princenoire) 
*   Лично Дмитрий Лейкин: сеть “Вконтакте” [https://vk.com/dilesoft](https://vk.com/dilesoft) . Телеграм: @dilesoft телефон +79031253368
*   По емайл: [protopia.home@gmail.com](mailto:protopia.home@gmail.com) 

С уважением, проект “[Дом Протопии](http://protopia-home.ru)”. 

